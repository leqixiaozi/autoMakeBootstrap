package com.zzwtel.autocode.beetl;

import java.util.List;

import org.beetl.core.Configuration;
import org.beetl.core.GroupTemplate;
import org.beetl.core.Template;
import org.beetl.core.resource.FileResourceLoader;

import com.zzwtel.autocode.template.constants.TemplatePath;
import com.zzwtel.autocode.template.model.Property;
import com.zzwtel.autocode.template.model.Table;
import com.zzwtel.autocode.template.model.UIModel;
import com.zzwtel.autocode.util.FileUtil;
import com.zzwtel.autocode.util.HumpUtil;
import com.zzwtel.autocode.util.ListUtil;
import com.zzwtel.autocode.util.PathUtil;
import com.zzwtel.autocode.util.StrKit;

/**
 * 编辑表单模板类,支持2列
 * @author yangtonggan
 * @date 2016-3-8
 */
public class EditForm2ColumnHtmlTemplate {
	/**
	 * 生成java代码
	 * @param m 数据模型
	 * @param template 模板路径
	 * 需要分清 首字母 大写 ，小写 ，全部大写 ，小写
	 */
	public void generate(UIModel m,String dir){
		try{			
			String root = PathUtil.getTemplateRoot();
			FileResourceLoader resourceLoader = new FileResourceLoader(root,"utf-8");
			Configuration cfg = Configuration.defaultConfiguration();
			GroupTemplate gt = new GroupTemplate(resourceLoader, cfg);
			
			Template t = gt.getTemplate(TemplatePath.EDIT_FORM_2COLUMN_HTML_TEMPLATE);	
			String model = m.getTable().getModel();			
			//首字母小写，驼峰命名
			t.binding("entityModel", model);
			//首字母大写，驼峰命名
			t.binding("EntityModel", StrKit.firstCharToUpperCase(model));
			//全部大写，下划线分割
			String _model = HumpUtil.underscoreName(model);
			t.binding("ENTITY_MODEL", _model);
			//全部小写，下划线分割			
			t.binding("entity_model", HumpUtil.toLowerCase(_model));	
			
			List<Property> list = m.getProps();
			//需要平均分配list
			List<List<Property>> array = ListUtil.split2List(list);			
			t.binding("fields1", array.get(0));	
			t.binding("fields2", array.get(1));
			
		
			String data = t.render();			
			String out = PathUtil.getOutRoot();			
			String fileName = StringTemplate.getFileName(TemplatePath.EDIT_FORM_2COLUMN_HTML_TEMPLATE,"entity_model", HumpUtil.toLowerCase(_model));		
			System.out.println(out+dir+fileName);
			FileUtil.write(out+dir+fileName, data);
		}catch(Exception e){
			throw new RuntimeException(e);
		}
	}
	
	public static void main(String[] args){
		AddFormHtmlTemplate jft = new AddFormHtmlTemplate();
		UIModel m = new UIModel();
		m.setTable(new Table());
		m.getTable().setModel("area");
		jft.generate(m, "system");
	}
}
