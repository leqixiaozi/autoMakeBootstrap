package com.zzwtel.autocode.builder;

import com.zzwtel.autocode.beetl.LeftMenuHtmlTemplate;
import com.zzwtel.autocode.beetl.LeftMenuJsTemplate;
import com.zzwtel.autocode.template.model.LeftMenuModel;

/**
 * 左菜单构造器
 * @author yangtontgan
 * @date 2016-6-8
 */
public class LeftMenuBuilder {
	public void makeCode(){
		LeftMenuModel menuModel = new LeftMenuModel();
		menuModel.parse();
		makeMenuViewHtml(menuModel);
		makeMenuEventJs(menuModel);
	}
	/**
	 * 菜单对应的事件响应js文件
	 */
	private void makeMenuEventJs(LeftMenuModel menuModel){
		LeftMenuJsTemplate eventJs = new LeftMenuJsTemplate();
		eventJs.generate(menuModel,"menu");
		System.out.println("正在生成菜单对应的事件响应js文件");
	}
	/**
	 * 菜单显示对应的html文件
	 */
	private void makeMenuViewHtml(LeftMenuModel menuModel){
		LeftMenuHtmlTemplate viewHtml = new LeftMenuHtmlTemplate();
		viewHtml.generate(menuModel,"menu");
		System.out.println("正在生成菜单显示对应的html文件");
	}
	
	public static void main(String[] args){
		LeftMenuBuilder menu = new LeftMenuBuilder();
		menu.makeCode();
	}
}
