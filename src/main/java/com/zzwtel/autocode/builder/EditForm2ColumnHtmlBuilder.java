package com.zzwtel.autocode.builder;

import com.zzwtel.autocode.beetl.EditForm2ColumnHtmlTemplate;
import com.zzwtel.autocode.template.model.UIModel;

/**
 * 编辑表单构造器--支持2列
 * @author yangtonggan
 * @date 2016-3-7
 */
public class EditForm2ColumnHtmlBuilder {

	public void makeCode(UIModel vm,String modularDir) {
		EditForm2ColumnHtmlTemplate template = new EditForm2ColumnHtmlTemplate();
		template.generate(vm, modularDir);
		
	}

}
