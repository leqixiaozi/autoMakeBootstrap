package com.zzwtel.autocode.builder;

import com.zzwtel.autocode.beetl.AddForm2ColumnHtmlTemplate;
import com.zzwtel.autocode.template.model.UIModel;

/**
 * 添加表单构造器--支持2列
 * @author yangtonggan
 * @date 2016-3-7
 */
public class AddForm2ColumnHtmlBuilder {
	
	public void makeCode(UIModel vm,String modularDir) {
		AddForm2ColumnHtmlTemplate afht = new AddForm2ColumnHtmlTemplate();
		afht.generate(vm, modularDir);
	}
}
