package com.zzwtel.autocode.builder;

import java.util.List;

import com.zzwtel.autocode.beetl.UrlConstantsTemplate;
import com.zzwtel.autocode.template.model.UrlModel;

public class UrlConstantsBuilder {
	
	public void makeCode(List<UrlModel> urls) {
		UrlConstantsTemplate template = new UrlConstantsTemplate();
		template.generate(urls);		
	}
}
