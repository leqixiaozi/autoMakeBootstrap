package com.zzwtel.autocode.template.model;

/**
 * 
 * @author yangtonggan
  <?xml version='1.0' encoding='UTF-8'?>
  <table name="account" model="account" class="com.pointercn.community.model.AccountModel">
	<property column="id" name="id" viewType="hidden" key="true"/>
	<property column="area_id" name="areaId" viewType="text" />
	<property column="area" name="area" type="text" />
	<property column="role_id" name="roleId" type="text" />
	<property column="name" name="name" type="text" />
	<property column="psw" name="psw" type="text" />
	<property column="level" name="level" type="text" />
   </table>
 */
public class Table {
	//表名
	private String name;
	//模型名称
	private String model;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public String getClazz() {
		return clazz;
	}
	public void setClazz(String clazz) {
		this.clazz = clazz;
	}
	//模型的类名称
	private String clazz;
	
	
}
