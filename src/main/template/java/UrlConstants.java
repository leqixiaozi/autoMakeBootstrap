package com.zzwtec.community.common;

<?
var date = date();
?>

/**
 * url请求常量配置
 * @author yangtonggan
 * @date @{date,"yyyy-MM-dd"}
 * 
 */

public interface UrlConstants {	
	<? for(url in urls){ ?>
	    //映射--@{url.entity_model}列表--请求地址
		public static final String URL_@{url.ENTITY_MODEL} = "/@{url.entity_model}";
		//映射--添加数据@{url.entity_model}--请求地址
		public static final String URL_@{url.ENTITY_MODEL}_ADD = "/@{url.entity_model}/add";
		//映射--删除数据@{url.entity_model}--请求地址
		public static final String URL_@{url.ENTITY_MODEL}_DEL = "/@{url.entity_model}/del";
		//映射--修改数据@{url.entity_model}--请求地址
		public static final String URL_@{url.ENTITY_MODEL}_UPD = "/@{url.entity_model}/upd";
		//映射--添加@{url.entity_model}界面表单--请求地址
		public static final String URL_@{url.ENTITY_MODEL}_ADD_FORM_UI = "/@{url.entity_model}/add/form_ui";
		//映射--编辑@{url.entity_model}界面表单--请求地址
		public static final String URL_@{url.ENTITY_MODEL}_EDIT_FORM_UI = "/@{url.entity_model}/edit/form_ui";
	
	<? } ?>	
	
	<? /** ?>
	//@{entity_model}列表
	public static final String URL_@{ENTITY_MODEL} = "/@{entity_model}";
	//添加@{entity_model}
	public static final String URL_@{ENTITY_MODEL}_ADD = "/@{entity_model}/add";
	//删除@{entity_model}
	public static final String URL_@{ENTITY_MODEL}_DEL = "/@{entity_model}/del";
	//修改@{entity_model}信息
	public static final String URL_@{ENTITY_MODEL}_UPD = "/@{entity_model}/upd";
	//获取添加界面表单
	public static final String URL_@{ENTITY_MODEL}_ADD_FORM_UI = "/@{entity_model}/add/form_ui";
	//获取编辑界面表单
	public static final String URL_@{ENTITY_MODEL}_EDIT_FORM_UI = "/@{entity_model}/edit/form_ui";	
	<? */ ?>
}
