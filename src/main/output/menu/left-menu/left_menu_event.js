$(function(){
    /**
     * 打开account信息列表
	 * 当用户点击account信息菜单项时，调用该函数 
	 * 菜单项目ID命名规范:menu-account-page
	 */
	$('#menu-account-page').parent().click(function(){
		$.ajax({
			type:"POST",
			cache: false,
			url:ctx+"/account/list",
			data:"",
			success:function(msg){
				addPage(msg);
			},
			complete:function(){
				waitingStop();
			},
			beforeSend:function(){
				waiting();
			}
		});
	});	
    /**
     * 打开account_log信息列表
	 * 当用户点击account_log信息菜单项时，调用该函数 
	 * 菜单项目ID命名规范:menu-account_log-page
	 */
	$('#menu-account_log-page').parent().click(function(){
		$.ajax({
			type:"POST",
			cache: false,
			url:ctx+"/account_log/list",
			data:"",
			success:function(msg){
				addPage(msg);
			},
			complete:function(){
				waitingStop();
			},
			beforeSend:function(){
				waiting();
			}
		});
	});	
    /**
     * 打开account_right信息列表
	 * 当用户点击account_right信息菜单项时，调用该函数 
	 * 菜单项目ID命名规范:menu-account_right-page
	 */
	$('#menu-account_right-page').parent().click(function(){
		$.ajax({
			type:"POST",
			cache: false,
			url:ctx+"/account_right/list",
			data:"",
			success:function(msg){
				addPage(msg);
			},
			complete:function(){
				waitingStop();
			},
			beforeSend:function(){
				waiting();
			}
		});
	});	
    /**
     * 打开ad信息列表
	 * 当用户点击ad信息菜单项时，调用该函数 
	 * 菜单项目ID命名规范:menu-ad-page
	 */
	$('#menu-ad-page').parent().click(function(){
		$.ajax({
			type:"POST",
			cache: false,
			url:ctx+"/ad/list",
			data:"",
			success:function(msg){
				addPage(msg);
			},
			complete:function(){
				waitingStop();
			},
			beforeSend:function(){
				waiting();
			}
		});
	});	
    /**
     * 打开ad_admin信息列表
	 * 当用户点击ad_admin信息菜单项时，调用该函数 
	 * 菜单项目ID命名规范:menu-ad_admin-page
	 */
	$('#menu-ad_admin-page').parent().click(function(){
		$.ajax({
			type:"POST",
			cache: false,
			url:ctx+"/ad_admin/list",
			data:"",
			success:function(msg){
				addPage(msg);
			},
			complete:function(){
				waitingStop();
			},
			beforeSend:function(){
				waiting();
			}
		});
	});	
    /**
     * 打开ad_admin_opt_log信息列表
	 * 当用户点击ad_admin_opt_log信息菜单项时，调用该函数 
	 * 菜单项目ID命名规范:menu-ad_admin_opt_log-page
	 */
	$('#menu-ad_admin_opt_log-page').parent().click(function(){
		$.ajax({
			type:"POST",
			cache: false,
			url:ctx+"/ad_admin_opt_log/list",
			data:"",
			success:function(msg){
				addPage(msg);
			},
			complete:function(){
				waitingStop();
			},
			beforeSend:function(){
				waiting();
			}
		});
	});	
    /**
     * 打开ad_admin_right信息列表
	 * 当用户点击ad_admin_right信息菜单项时，调用该函数 
	 * 菜单项目ID命名规范:menu-ad_admin_right-page
	 */
	$('#menu-ad_admin_right-page').parent().click(function(){
		$.ajax({
			type:"POST",
			cache: false,
			url:ctx+"/ad_admin_right/list",
			data:"",
			success:function(msg){
				addPage(msg);
			},
			complete:function(){
				waitingStop();
			},
			beforeSend:function(){
				waiting();
			}
		});
	});	
    /**
     * 打开ad_log信息列表
	 * 当用户点击ad_log信息菜单项时，调用该函数 
	 * 菜单项目ID命名规范:menu-ad_log-page
	 */
	$('#menu-ad_log-page').parent().click(function(){
		$.ajax({
			type:"POST",
			cache: false,
			url:ctx+"/ad_log/list",
			data:"",
			success:function(msg){
				addPage(msg);
			},
			complete:function(){
				waitingStop();
			},
			beforeSend:function(){
				waiting();
			}
		});
	});	
    /**
     * 打开admdevice信息列表
	 * 当用户点击admdevice信息菜单项时，调用该函数 
	 * 菜单项目ID命名规范:menu-admdevice-page
	 */
	$('#menu-admdevice-page').parent().click(function(){
		$.ajax({
			type:"POST",
			cache: false,
			url:ctx+"/admdevice/list",
			data:"",
			success:function(msg){
				addPage(msg);
			},
			complete:function(){
				waitingStop();
			},
			beforeSend:function(){
				waiting();
			}
		});
	});	
    /**
     * 打开admin信息列表
	 * 当用户点击admin信息菜单项时，调用该函数 
	 * 菜单项目ID命名规范:menu-admin-page
	 */
	$('#menu-admin-page').parent().click(function(){
		$.ajax({
			type:"POST",
			cache: false,
			url:ctx+"/admin/list",
			data:"",
			success:function(msg){
				addPage(msg);
			},
			complete:function(){
				waitingStop();
			},
			beforeSend:function(){
				waiting();
			}
		});
	});	
    /**
     * 打开admin_log信息列表
	 * 当用户点击admin_log信息菜单项时，调用该函数 
	 * 菜单项目ID命名规范:menu-admin_log-page
	 */
	$('#menu-admin_log-page').parent().click(function(){
		$.ajax({
			type:"POST",
			cache: false,
			url:ctx+"/admin_log/list",
			data:"",
			success:function(msg){
				addPage(msg);
			},
			complete:function(){
				waitingStop();
			},
			beforeSend:function(){
				waiting();
			}
		});
	});	
    /**
     * 打开admin_right信息列表
	 * 当用户点击admin_right信息菜单项时，调用该函数 
	 * 菜单项目ID命名规范:menu-admin_right-page
	 */
	$('#menu-admin_right-page').parent().click(function(){
		$.ajax({
			type:"POST",
			cache: false,
			url:ctx+"/admin_right/list",
			data:"",
			success:function(msg){
				addPage(msg);
			},
			complete:function(){
				waitingStop();
			},
			beforeSend:function(){
				waiting();
			}
		});
	});	
    /**
     * 打开admin_role信息列表
	 * 当用户点击admin_role信息菜单项时，调用该函数 
	 * 菜单项目ID命名规范:menu-admin_role-page
	 */
	$('#menu-admin_role-page').parent().click(function(){
		$.ajax({
			type:"POST",
			cache: false,
			url:ctx+"/admin_role/list",
			data:"",
			success:function(msg){
				addPage(msg);
			},
			complete:function(){
				waitingStop();
			},
			beforeSend:function(){
				waiting();
			}
		});
	});	
    /**
     * 打开advertiser信息列表
	 * 当用户点击advertiser信息菜单项时，调用该函数 
	 * 菜单项目ID命名规范:menu-advertiser-page
	 */
	$('#menu-advertiser-page').parent().click(function(){
		$.ajax({
			type:"POST",
			cache: false,
			url:ctx+"/advertiser/list",
			data:"",
			success:function(msg){
				addPage(msg);
			},
			complete:function(){
				waitingStop();
			},
			beforeSend:function(){
				waiting();
			}
		});
	});	
    /**
     * 打开alert_device信息列表
	 * 当用户点击alert_device信息菜单项时，调用该函数 
	 * 菜单项目ID命名规范:menu-alert_device-page
	 */
	$('#menu-alert_device-page').parent().click(function(){
		$.ajax({
			type:"POST",
			cache: false,
			url:ctx+"/alert_device/list",
			data:"",
			success:function(msg){
				addPage(msg);
			},
			complete:function(){
				waitingStop();
			},
			beforeSend:function(){
				waiting();
			}
		});
	});	
    /**
     * 打开alert_log信息列表
	 * 当用户点击alert_log信息菜单项时，调用该函数 
	 * 菜单项目ID命名规范:menu-alert_log-page
	 */
	$('#menu-alert_log-page').parent().click(function(){
		$.ajax({
			type:"POST",
			cache: false,
			url:ctx+"/alert_log/list",
			data:"",
			success:function(msg){
				addPage(msg);
			},
			complete:function(){
				waitingStop();
			},
			beforeSend:function(){
				waiting();
			}
		});
	});	
    /**
     * 打开area信息列表
	 * 当用户点击area信息菜单项时，调用该函数 
	 * 菜单项目ID命名规范:menu-area-page
	 */
	$('#menu-area-page').parent().click(function(){
		$.ajax({
			type:"POST",
			cache: false,
			url:ctx+"/area/list",
			data:"",
			success:function(msg){
				addPage(msg);
			},
			complete:function(){
				waitingStop();
			},
			beforeSend:function(){
				waiting();
			}
		});
	});	
    /**
     * 打开build信息列表
	 * 当用户点击build信息菜单项时，调用该函数 
	 * 菜单项目ID命名规范:menu-build-page
	 */
	$('#menu-build-page').parent().click(function(){
		$.ajax({
			type:"POST",
			cache: false,
			url:ctx+"/build/list",
			data:"",
			success:function(msg){
				addPage(msg);
			},
			complete:function(){
				waitingStop();
			},
			beforeSend:function(){
				waiting();
			}
		});
	});	
    /**
     * 打开call_log信息列表
	 * 当用户点击call_log信息菜单项时，调用该函数 
	 * 菜单项目ID命名规范:menu-call_log-page
	 */
	$('#menu-call_log-page').parent().click(function(){
		$.ajax({
			type:"POST",
			cache: false,
			url:ctx+"/call_log/list",
			data:"",
			success:function(msg){
				addPage(msg);
			},
			complete:function(){
				waitingStop();
			},
			beforeSend:function(){
				waiting();
			}
		});
	});	
    /**
     * 打开cell信息列表
	 * 当用户点击cell信息菜单项时，调用该函数 
	 * 菜单项目ID命名规范:menu-cell-page
	 */
	$('#menu-cell-page').parent().click(function(){
		$.ajax({
			type:"POST",
			cache: false,
			url:ctx+"/cell/list",
			data:"",
			success:function(msg){
				addPage(msg);
			},
			complete:function(){
				waitingStop();
			},
			beforeSend:function(){
				waiting();
			}
		});
	});	
    /**
     * 打开community信息列表
	 * 当用户点击community信息菜单项时，调用该函数 
	 * 菜单项目ID命名规范:menu-community-page
	 */
	$('#menu-community-page').parent().click(function(){
		$.ajax({
			type:"POST",
			cache: false,
			url:ctx+"/community/list",
			data:"",
			success:function(msg){
				addPage(msg);
			},
			complete:function(){
				waitingStop();
			},
			beforeSend:function(){
				waiting();
			}
		});
	});	
    /**
     * 打开device_opt_log信息列表
	 * 当用户点击device_opt_log信息菜单项时，调用该函数 
	 * 菜单项目ID命名规范:menu-device_opt_log-page
	 */
	$('#menu-device_opt_log-page').parent().click(function(){
		$.ajax({
			type:"POST",
			cache: false,
			url:ctx+"/device_opt_log/list",
			data:"",
			success:function(msg){
				addPage(msg);
			},
			complete:function(){
				waitingStop();
			},
			beforeSend:function(){
				waiting();
			}
		});
	});	
    /**
     * 打开door_card信息列表
	 * 当用户点击door_card信息菜单项时，调用该函数 
	 * 菜单项目ID命名规范:menu-door_card-page
	 */
	$('#menu-door_card-page').parent().click(function(){
		$.ajax({
			type:"POST",
			cache: false,
			url:ctx+"/door_card/list",
			data:"",
			success:function(msg){
				addPage(msg);
			},
			complete:function(){
				waitingStop();
			},
			beforeSend:function(){
				waiting();
			}
		});
	});	
    /**
     * 打开doorbell信息列表
	 * 当用户点击doorbell信息菜单项时，调用该函数 
	 * 菜单项目ID命名规范:menu-doorbell-page
	 */
	$('#menu-doorbell-page').parent().click(function(){
		$.ajax({
			type:"POST",
			cache: false,
			url:ctx+"/doorbell/list",
			data:"",
			success:function(msg){
				addPage(msg);
			},
			complete:function(){
				waitingStop();
			},
			beforeSend:function(){
				waiting();
			}
		});
	});	
    /**
     * 打开enclosure信息列表
	 * 当用户点击enclosure信息菜单项时，调用该函数 
	 * 菜单项目ID命名规范:menu-enclosure-page
	 */
	$('#menu-enclosure-page').parent().click(function(){
		$.ajax({
			type:"POST",
			cache: false,
			url:ctx+"/enclosure/list",
			data:"",
			success:function(msg){
				addPage(msg);
			},
			complete:function(){
				waitingStop();
			},
			beforeSend:function(){
				waiting();
			}
		});
	});	
    /**
     * 打开msg信息列表
	 * 当用户点击msg信息菜单项时，调用该函数 
	 * 菜单项目ID命名规范:menu-msg-page
	 */
	$('#menu-msg-page').parent().click(function(){
		$.ajax({
			type:"POST",
			cache: false,
			url:ctx+"/msg/list",
			data:"",
			success:function(msg){
				addPage(msg);
			},
			complete:function(){
				waitingStop();
			},
			beforeSend:function(){
				waiting();
			}
		});
	});	
    /**
     * 打开open_door信息列表
	 * 当用户点击open_door信息菜单项时，调用该函数 
	 * 菜单项目ID命名规范:menu-open_door-page
	 */
	$('#menu-open_door-page').parent().click(function(){
		$.ajax({
			type:"POST",
			cache: false,
			url:ctx+"/open_door/list",
			data:"",
			success:function(msg){
				addPage(msg);
			},
			complete:function(){
				waitingStop();
			},
			beforeSend:function(){
				waiting();
			}
		});
	});	
    /**
     * 打开outdevice信息列表
	 * 当用户点击outdevice信息菜单项时，调用该函数 
	 * 菜单项目ID命名规范:menu-outdevice-page
	 */
	$('#menu-outdevice-page').parent().click(function(){
		$.ajax({
			type:"POST",
			cache: false,
			url:ctx+"/outdevice/list",
			data:"",
			success:function(msg){
				addPage(msg);
			},
			complete:function(){
				waitingStop();
			},
			beforeSend:function(){
				waiting();
			}
		});
	});	
    /**
     * 打开property信息列表
	 * 当用户点击property信息菜单项时，调用该函数 
	 * 菜单项目ID命名规范:menu-property-page
	 */
	$('#menu-property-page').parent().click(function(){
		$.ajax({
			type:"POST",
			cache: false,
			url:ctx+"/property/list",
			data:"",
			success:function(msg){
				addPage(msg);
			},
			complete:function(){
				waitingStop();
			},
			beforeSend:function(){
				waiting();
			}
		});
	});	
    /**
     * 打开role信息列表
	 * 当用户点击role信息菜单项时，调用该函数 
	 * 菜单项目ID命名规范:menu-role-page
	 */
	$('#menu-role-page').parent().click(function(){
		$.ajax({
			type:"POST",
			cache: false,
			url:ctx+"/role/list",
			data:"",
			success:function(msg){
				addPage(msg);
			},
			complete:function(){
				waitingStop();
			},
			beforeSend:function(){
				waiting();
			}
		});
	});	
    /**
     * 打开systoken信息列表
	 * 当用户点击systoken信息菜单项时，调用该函数 
	 * 菜单项目ID命名规范:menu-systoken-page
	 */
	$('#menu-systoken-page').parent().click(function(){
		$.ajax({
			type:"POST",
			cache: false,
			url:ctx+"/systoken/list",
			data:"",
			success:function(msg){
				addPage(msg);
			},
			complete:function(){
				waitingStop();
			},
			beforeSend:function(){
				waiting();
			}
		});
	});	
    /**
     * 打开token_key信息列表
	 * 当用户点击token_key信息菜单项时，调用该函数 
	 * 菜单项目ID命名规范:menu-token_key-page
	 */
	$('#menu-token_key-page').parent().click(function(){
		$.ajax({
			type:"POST",
			cache: false,
			url:ctx+"/token_key/list",
			data:"",
			success:function(msg){
				addPage(msg);
			},
			complete:function(){
				waitingStop();
			},
			beforeSend:function(){
				waiting();
			}
		});
	});	
    /**
     * 打开user信息列表
	 * 当用户点击user信息菜单项时，调用该函数 
	 * 菜单项目ID命名规范:menu-user-page
	 */
	$('#menu-user-page').parent().click(function(){
		$.ajax({
			type:"POST",
			cache: false,
			url:ctx+"/user/list",
			data:"",
			success:function(msg){
				addPage(msg);
			},
			complete:function(){
				waitingStop();
			},
			beforeSend:function(){
				waiting();
			}
		});
	});	
    /**
     * 打开user_set信息列表
	 * 当用户点击user_set信息菜单项时，调用该函数 
	 * 菜单项目ID命名规范:menu-user_set-page
	 */
	$('#menu-user_set-page').parent().click(function(){
		$.ajax({
			type:"POST",
			cache: false,
			url:ctx+"/user_set/list",
			data:"",
			success:function(msg){
				addPage(msg);
			},
			complete:function(){
				waitingStop();
			},
			beforeSend:function(){
				waiting();
			}
		});
	});	
    /**
     * 打开zone信息列表
	 * 当用户点击zone信息菜单项时，调用该函数 
	 * 菜单项目ID命名规范:menu-zone-page
	 */
	$('#menu-zone-page').parent().click(function(){
		$.ajax({
			type:"POST",
			cache: false,
			url:ctx+"/zone/list",
			data:"",
			success:function(msg){
				addPage(msg);
			},
			complete:function(){
				waitingStop();
			},
			beforeSend:function(){
				waiting();
			}
		});
	});	
    /**
     * 打开信息列表
	 * 当用户点击信息菜单项时，调用该函数 
	 * 菜单项目ID命名规范:menu-xxxx-page
	 */
	$('#menu-xxxx-page').parent().click(function(){
		$.ajax({
			type:"POST",
			cache: false,
			url:ctx+"//list",
			data:"",
			success:function(msg){
				addPage(msg);
			},
			complete:function(){
				waitingStop();
			},
			beforeSend:function(){
				waiting();
			}
		});
	});	
	
});



/**
 * 向工作区域添加面板
 */
function addPage(msg) {	
	$("#work_area").empty();
	$("#work_area").append(msg);
	scrollPane();	
}
