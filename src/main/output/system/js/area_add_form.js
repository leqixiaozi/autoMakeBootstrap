$(function(){
	/**设置模态框底部按钮颜色*/
	$('#btn_ok').mouseover(function(){
		$('#btn_cancel').css({
			'background-color':'#fff',
			'color':'#90b0e4'
		});		
		$(this).css({
			'background-color':'#90b0e4',
			'color':'#fff'
		});		
	}); 
	/**设置模态框底部按钮颜色*/
	$('#btn_cancel').mouseover(function(){		
		$('#btn_ok').css({
			'background-color':'#fff',
			'color':'#90b0e4'
		});		
		$(this).css({
			'background-color':'#90b0e4',
			'color':'#fff'
		});
	});
	
	/**
	 * 点击关闭按钮，关闭当前弹出窗口
	 */
	$('#btn_cancel').click(function(){		
		if($("div.popup_window div#btn_cancel").length >= 0){
			$("div.popup_window").remove();
		} 		
	});
	
	/**
	 * 点击表单ok按钮
	 */
	$('#btn_ok').click(function(){	
		var inputs = $("form[name=area_add_from] :input");	
		var data = {};		
		$(inputs).each(function(){
			var input = $(this);			
			data[input.attr('name')]=input.val();			
		});			
		$.ajax({
			type:"POST",
			url:ctx+"/area/add",
			data:data,
			success:function(repJson){				
				openTipLayer(repJson.msg);
			}
		});
		
	});
	
	
});

