package com.zzwtec.community.action.system;


import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;

import com.jfinal.core.ActionKey;
import com.jfinal.core.Controller;
import com.zzwtec.community.common.DataObject;
import com.zzwtec.community.common.IceServiceUtil;
import com.zzwtec.community.common.Page;
import com.zzwtec.community.common.UrlConstants;
import com.zzwtec.community.model.Area;
import com.zzwtec.community.model.BuildM;
import com.zzwtec.community.model.AccountRight;
import com.zzwtec.community.model.AccountRightM;
import com.zzwtec.community.model.AccountRightMPage;
import com.zzwtec.community.model.CommunityM;
import com.zzwtec.community.model.CommunityMPage;
import com.zzwtec.community.service.BuildServicePrx;
import com.zzwtec.community.service.AccountRightServicePrx;
import com.zzwtec.community.service.CommunityServicePrx;

/**
 * AccountRight控制器
 * @author yangtonggan
 * @date  2016-04-01
 * 
 */
public class AccountRightController extends Controller {
	/**
	 * 获取区域列表
	 */
	@ActionKey(UrlConstants.URL_ACCOUNT_RIGHT)
	public void list(){				
		//AccountRightServicePrx prx = IceServiceUtil.getService(AccountRightServicePrx.class);
		//query(prx);
		// 构造测试数据	
		buildTestData();
		render("/system/view/account_right_list.html");
	}
	
	/**
	 * 添加AccountRight
	 */
	@ActionKey(UrlConstants.URL_ACCOUNT_RIGHT_ADD)
	public void add(){		
		DataObject repJson = new DataObject("添加AccountRight成功");;
		try{
			//AccountRightServicePrx prx = IceServiceUtil.getService(AccountRightServicePrx.class);
			AccountRight model = getBean(AccountRight.class);		
			//prx.addAccountRight(model);				
		}catch(Exception e){
			repJson = new DataObject("添加AccountRight失败");
		}finally{
			renderJson(repJson);
		}	
		
	}
	
	/**
	 * 删除AccountRight
	 */
	@ActionKey(UrlConstants.URL_ACCOUNT_RIGHT_DEL)
	public void del(){		
		DataObject repJson = new DataObject("删除AccountRight成功");
		try{
			String ids = getPara("ids");		
			//AccountRightServicePrx prx = IceServiceUtil.getService(AccountRightServicePrx.class);
			//prx.delAccountRightByIds(ids.split(","));
		}catch(Exception e){
			repJson = new DataObject("删除AccountRight失败");
		}finally{
			renderJson(repJson);
		}
	}
	
	/**
	 * 获取添加表单界面
	 */
	@ActionKey(UrlConstants.URL_ACCOUNT_RIGHT_ADD_FORM_UI)
	public void addFormUI(){
		//TODO此处可以做一些预处理，比如为下拉列表赋值
		AccountRight entity = new AccountRight();	
		setAttr("accountRight", entity);
		render("/system/view/account_right_add_form.html");
	}
	
	/**
	 * 根据ID获取一条记录,并且返回编辑界面UI
	 */
	@ActionKey(UrlConstants.URL_ACCOUNT_RIGHT_EDIT_FORM_UI)
	public void editFormUI(){	
		try{			
			String id = getPara("id");	
			//AccountRightServicePrx prx = IceServiceUtil.getService(AccountRightServicePrx.class);
			//AccountRightM modele = prx.inspectAccountRight(id);
			AccountRight entity = new AccountRight();		
			//BeanUtils.copyProperties(modele, entity);	
			//TODO需要对界面下拉列表做预处理
			setAttr("accountRight", entity);			
			render("/system/view/account_right_edit_form.html");
		}catch(Exception e){
			DataObject repJson = new DataObject("获取AccountRight失败");
			renderJson(repJson);
		}
	}	
	
	
	/**
	 * 修改AccountRight
	 */
	@ActionKey(UrlConstants.URL_ACCOUNT_RIGHT_UPD)
	public void upd(){
		DataObject repJson = new DataObject("更新AccountRight成功");
		try{
			//AccountRightServicePrx prx = IceServiceUtil.getService(AccountRightServicePrx.class);
			AccountRight model = getBean(AccountRight.class);
			//prx.alterAccountRight(model);
		}catch(Exception e){
			repJson = new DataObject("更新AccountRight失败");
		}finally{
			renderJson(repJson);
		}		
	}
	
	private void query(AccountRightServicePrx prx){		
		Page page = new Page();		
		Map<String, String> term = new HashMap<String, String>();
		/*
		String name = getPara("name");
		term.put("name", name);
		//请参考注释代码设置查询条件
		*/
		//AccountRightMPage viewModel = prx.getAccountRightList(page, term);			
		//setAttr("viewData", viewModel);		
	}	
	
	private void buildTestData(){		
		AccountRight[] aray = new AccountRight[12];		
		for(int i=0;i<12;i++){
			aray[i] = new AccountRight();
			aray[i].id = "id-"+i;			
		}	
		List<AccountRight> pageList = Arrays.asList(aray);		
		setAttr("pageList", pageList);		
	}
	
}
